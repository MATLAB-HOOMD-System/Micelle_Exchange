function [aggregatelist,I,R,Mn,time,tau]=FindRate(xmlname,dcdname,deltat,savename,path,numworkers,saveflag,extract_steps,equilstep,mixedtype,cutoff)

if exist('cutoff','var')==0
    cutoff=1.5;
end

%nsteps=[start step
% % addpath('~/submissions/A2B3a55_8_12_16')
if exist('path')~=0
    if path ~=0
        addpath(path)
    end
end
addpath('matlabcode')
addpath('matlabcode/matdcd-1.0')
% clear all
% totsteps=nsteps(end);
% deltat=0.04*10000;

% clc
% system=Extract_XML_DCD('A2B3a55_8_12_16_1_start.xml','A2B3a55_8_12_16_1_reallyfasttraj.dcd',0,0);

system=Extract_XML_DCD(xmlname,dcdname,0,extract_steps);
if mixedtype==1
    system=strip_beads(0,system,'A-B')
elseif mixedtype==2
    system.attype(system.attype=='C' | system.attype=='E' )='B';
    system.attype(system.attype=='D')='A';
    system=strip_beads(0,system,'A-B')
elseif mixedtype==3
    system.attype(system.attype=='A' | system.attype=='B' )='Z';
    system.attype(system.attype=='C' | system.attype=='E' )='B';
    system.attype(system.attype=='D')='A';
    system=strip_beads(0,system,'A-B')
elseif mixedtype==3
    system.attype(system.attype=='C')='A';
    system.attype(system.attype=='D')='B';
    system.attype(system.attype=='E')='B';
    system.attype(system.attype=='F')='C';
    system=strip_beads(0,system,'A-B')
end
% system=trimsystem(system,1000)
system2=system;
systemexpand_script;

% pos=packing(pos(:,:,:),bonds,dim,0) ;
system=systemcompress(attype,bonds,dim,mass,pos,angles);
systemB=strip_beads(0,system,'B');  %'B'
system=strip_beads(0,system);
systemexpand_script;

chainstats=Chain_Statistics_single(0,system);
chainstats2=Chain_Statistics_single(0,system2);
% chainstats2all=Chain_Statistics(0,system2);
chainstatsB=Chain_Statistics_single(0,systemB);
% chainstats=Chain_Statistics(0,system);
systemcompress_script

% delete(gcp('nocreate'))
posall=pos;
[a,b,t]=size(pos);
% if exist('numworkers')
%     if numworkers~=0
%         P=parpool(numworkers)
%     end
% end
% load('test')
% system=systemcompress(attype,bonds,dim,mass,pos,angles);

counter=0;



% needed to make parfor work right
attype=attype;
bonds=bonds;
dim=dim;
mass=mass;
angles=angles;
chainstats=chainstats;

 disp(['starting  ' xmlname])

%  save('FindRateVars')
parfor i=1:length(extract_steps) %%parfor  %%%%aggregate list I think%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp(['start timestep' num2str(i)])
    starttic=tic;
    if mod(i,100)==0
        disp([num2str(i) '  ' xmlname])
    end
    pos=posall(:,:,i);
    system=systemcompress(attype,bonds,dim,mass,pos,angles,chainstats);
    chainstats2=Chain_Statistics_single(0,system);
    aggregatelist(i)=calc_aggregates_beads(0,system,cutoff);
    aggregatelist(i).chainstats=chainstats2;
    disp(['end timestep' num2str(i)])
    toc(starttic)
%     aggregatelist2=aggregatelist;
%     save('testagglist','aggregatelist2')
end
% disp('pausing now')
% pause(1e6)

% calculate B chainstats
parfor i=1:length(extract_steps)
    systemB2=systemB;
    systemB2.pos=systemB2.pos(:,:,i)
    chainstatsB=Chain_Statistics_single(0,systemB2);
    aggregatelist(i).chainstatsB=chainstatsB;
end


% system=systemcompress(attype,bonds,dim,mass,posall,angles,chainstats);

% aggregatelist(1)
% aggregatelist(400)

for i=1:length(aggregatelist)
    Mn(i)=aggregatelist(i).Mn;
end



%%%%%%%%%%%starting calculation of kinetics
systemexpand_script
equilaggregates=aggregatelist(equilstep).aggbeads;
% aggregatelist(100).aggbeads
s=size(aggregatelist(equilstep).aggbeads);

for j=2:2:s(2)
    for i=1:s(1)
        if equilaggregates(i,j)~=0
            attype(equilaggregates(i,j))='B';
        end
    end
end

systemcompress_script;

posall=pos;
pos=posall(:,:,1);
systemcompress_script
if exist('saveflag')~=0
    if saveflag==1
        Write_XML(0,[savename '_recolored.xml'],system)
        writedcd([savename '_recolored.dcd'],posall);
    end
end

pos=posall;
systemcompress_script

Iall=zeros(1,length(equilstep:length(extract_steps)));
Iendall=Iall;

% for E=equilstep:length(extract_steps)
%     clear I Iendaveraged N Nr Nr2
%     s=size(aggregatelist(E).aggbeads);
%     Eaggregates=aggregatelist(E).aggbeads;
%     attype(:)='A';
%     for j=2:2:s(2)
%         for i=1:s(1)
%             if Eaggregates(i,j)~=0
%                 attype(Eaggregates(i,j))='B';
%             end
%         end
%     end
    for k=equilstep:length(extract_steps)
    s=size(aggregatelist(k).aggbeads);
    Nt=length(attype);
        for j=1:chainstats.nchains
            if rand<sum(attype=='B')/length(attype)
                attyperand(chainstats.chains2(:,j))='B';
            else
                attyperand(chainstats.chains2(:,j))='A';
            end
        end
        for j=1:s(2)
            Nr(j)=0;
            Nr2(j)=0;
            N(j)=0;
            for i=1:s(1)
                if aggregatelist(k).aggbeads(i,j)~=0
                    N(j)=N(j)+1;
                    if attype(aggregatelist(k).aggbeads(i,j))=='B'
    %                     temp=Nr(j);
                        Nr(j)=Nr(j)+1;
                    end
                    if attyperand(aggregatelist(k).aggbeads(i,j))=='B'
    %                     temp=Nr(j);
                        Nr2(j)=Nr2(j)+1;
                    end
    %                 if rand<sum(attype=='B')/length(attype)
    %                     Nr2(j)=Nr2(j)+1;
    %                 end
                end
            end
        end
        I(k-equilstep+1)=0;
        Iendaveraged(k-equilstep+1)=0;

        I(k-equilstep+1)=sum((((Nr./N-0.5).^2)*4).*N./Nt);
        Iendaveraged(k-equilstep+1)=sum((((Nr2./N-0.5).^2)*4).*N./Nt);
     
    %     for j=1:s(2)
    %         I(k-equilstep+1)=I(k-equilstep+1)+4*((Nr(j)/N(j)-0.5)^2)*N(j)/Nt;
    %         Iendaveraged(k-equilstep+1)=Iendaveraged(k-equilstep+1)+4*((Nr2(j)/N(j)-0.5)^2)*N(j)/Nt;
    %     end

    % if k>30
    %     close all
    %     [A,bins]=hist(Nr./N);
    %     [B,bins2]=hist(Nr2./N)
    %     bar(bins,[A;B]')
    % end
    end
%     Iall(1:length(I))=Iall(1:length(I))+I;
%     Iendall(1:length(I))=Iendall(1:length(I))+Iendaveraged;
% end
% divisor=1:length(Iall);
% divisor=divisor(end:-1:1);
% Iall=Iall./divisor;
% Iendall=Iendall./divisor;
% I=Iall;
% Iendaveraged=Iendall;

% close all



time=(1:length(I))*deltat;
timetot=[0:t-1]*deltat;
% figure
% plot(timetot,Mn)
% xlabel('time')
% ylabel('Mn')

% figure
% semilogy(time,I)
% xlabel('time')
% ylabel('I')
Iendaveraged2=mean(Iendaveraged);

% R=real(((I-I(end))./(I(1)-I(end))).^0.5);
R=real(((I-Iendaveraged2)./(I(1)-Iendaveraged2)).^0.5);
timefit=time;
Rfit=R;
timefit=timefit(1:10);
Rfit=Rfit(1:10);
% timefit=time(time<1000);
% Rfit=R((time<1000));
try
    r=fit(timefit',Rfit','exp1');
catch ME
    r=-1;
    cprintf('Errors',char(ME.message))
end
if isa(r,'float')
    tau=-1;

else
    tau=-1/r.b;
%     figure
%     semilogy(time,R,'r*',time,r.a*exp(r.b*time),'k')
%     xlabel('time')
%     ylabel('R')
end

% axis([1 300 0.1 1])


%%%calculate distribution of Mn
[num_occurances,Nagg_bin]=hist(([aggregatelist(equilstep:end).agglen]),1:100);
% figure
% plot(Nagg_bin(1:30),num_occurances(1:30))
% xlabel('Nagg')
% ylabel('#')
% ylim([0 100])

%%%%%%%%calculate unimer fraction
% totalsteps=length(aggregatelist(equilstep:end));
unimerfrac=num_occurances(1)/sum(num_occurances.*Nagg_bin);

%%%%%%%%%%corona concentration



% com=COM(Chain,pos,mass)
%systemexapnd not recolored


% posA=pos;
% if isfield(system2,'chainstats')
%     [attype,bonds,dim,mass,pos,angles,chainstats]=systemexpand(system2);
% else
%     [attype,bonds,dim,mass,pos,angles]=systemexpand(system2);
% end
% parfor t=equilstep:length(extract_steps)
%     totalnumBbeads=zeros(1,1001);
%     for m=1:aggregatelist(t).numaggs
%         com=COM(aggregatelist(t).aggbeads(aggregatelist(t).aggbeads(:,m)~=0,m),system.pos(:,:,t));
%         Bpos=system2.pos(system2.attype=='B',:,t);
%         coms=repmat(com,length(Bpos),1);
%         distances=sort(sum((coms-Bpos).^2,2)).^0.5;
%         [numBbeads,radialshell]=hist((distances),0:0.1:100);
%         totalnumBbeads=totalnumBbeads+numBbeads;
% %         hist(distances,1:0.1:50)
%     end
%     Coronaden(t,:)=totalnumBbeads/aggregatelist(t).numaggs;
% end
% Coronaden=sum(Coronaden,1)/length(equilstep:length(extract_steps));
% radialshell=0:0.1:100;
% radialvolume=(4/3)*pi*((radialshell.^3)-((radialshell-0.1).^3));
% Coronaden=Coronaden./radialvolume;
%
% figure
% plot(radialshell,Coronaden)

flagtype='C';
lasttype='A';
if system2.attype(1)=='A' %it's a diblock
    for i=1:length(system2.attype)
        if system2.attype(i)=='A' && lasttype=='B'
            if flagtype=='C'
                flagtype='D';
            else
                flagtype='C';
            end
            lasttype='A';
        elseif system2.attype(i)=='B'
            system2.attype(i)=flagtype;
            if lasttype=='A'
                lasttype='B';
            end
        end
    end
else %%%% it's a triblock
    B1=find(system2.attype=='A',1)-1;
    BpC=find(chainstats2.chain>1,1)-1; %beadsperchain
    BT=sum(system2.attype(1:BpC)=='B');
    B2=BT-B1;
    countdown=0;
    for i=1:length(system2.attype)
        if system2.attype(i)=='A'
            countdown=B2;
        elseif countdown>0
            system2.attype(i)='C';
            countdown=countdown-1;
        else
            system2.attype(i)='D';
        end
    end
end


if exist('dencalc','var') 
    if dencalc==1
        for t=equilstep:length(extract_steps) %parfor
            totalnumAbeads=zeros(1,1001);
            totalnumBbeads=zeros(1,1001);
            totalnumCbeads=zeros(1,1001);
            totalnumDbeads=zeros(1,1001);
            totalaggs=0;
        %     t
            for m=1:aggregatelist(t).numaggs
        %         t
        %         m
                com=COM(aggregatelist(t).aggbeads(aggregatelist(t).aggbeads(:,m)~=0,m),system.pos(:,:,t));
                aggchains=aggregatelist(t).aggregates(aggregatelist(t).aggregates(:,m)~=0,m);
        %         chainstats2.chains2(:,aggchains(2))
        %         Apos
                if length(aggchains)~=1
                    totalaggs=totalaggs+1;
                    for p=1:length(aggchains)  %p is aggregate number
                        if p==1
                            Apos=system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='A',aggchains(p)),:,t);
                            Bpos=system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='C' | system2.attype(chainstats2.chains2(:,aggchains(p)))=='D',aggchains(p)),:,t);
                            Cpos=system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='C',aggchains(p)),:,t);
                            Dpos=system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='D',aggchains(p)),:,t);%%%%%pos of all C beads on that chain
                        else
                            Apos=[Apos; system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='A',aggchains(p)),:,t)];
                            Bpos=[Bpos; system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='C' | system2.attype(chainstats2.chains2(:,aggchains(p)))=='D',aggchains(p)),:,t)];
                            Cpos=[Cpos; system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='C',aggchains(p)),:,t)];
                            Dpos=[Dpos; system2.pos(chainstats2.chains2(system2.attype(chainstats2.chains2(:,aggchains(p)))=='D',aggchains(p)),:,t)];
                        end%%%%%pos of all C beads on that chain
                    end

            %         Apos=system2.pos(system2.attype=='A',:,t);
            %         Bpos=system2.pos(system2.attype=='C' | system2.attype=='D',:,t);
            %         Cpos=system2.pos(system2.attype=='C',:,t);
            %         Dpos=system2.pos(system2.attype=='D',:,t);
                    sizeApos=size(Apos);
                    sizeBpos=size(Bpos);
                    sizeCpos=size(Cpos);
                    sizeDpos=size(Dpos);
                    comsA=repmat(com,sizeApos(1),1);
                    comsB=repmat(com,sizeBpos(1),1);
                    comsC=repmat(com,sizeCpos(1),1);
                    comsD=repmat(com,sizeDpos(1),1);

                    distancesA=calcdistancesfindrate(comsA,Apos,dim);
                    distancesB=calcdistancesfindrate(comsB,Bpos,dim);
                    distancesC=calcdistancesfindrate(comsC,Cpos,dim);
                    distancesD=calcdistancesfindrate(comsD,Dpos,dim);

        %             distancesA=sort(sum((comsA-Apos).^2,2)).^0.5;
        %             distancesB=sort(sum((comsB-Bpos).^2,2)).^0.5;
        %             distancesC=sort(sum((comsC-Cpos).^2,2)).^0.5;
        %             distancesD=sort(sum((comsD-Dpos).^2,2)).^0.5;
                    [numAbeads,radialshell]=hist((distancesA),0:0.1:100);
                    [numBbeads,radialshell]=hist((distancesB),0:0.1:100);
                    [numCbeads,radialshell]=hist((distancesC),0:0.1:100);
                    [numDbeads,radialshell]=hist((distancesD),0:0.1:100);
                    totalnumAbeads=totalnumAbeads+numAbeads;
                    totalnumBbeads=totalnumBbeads+numBbeads;
                    totalnumCbeads=totalnumCbeads+numCbeads;
                    totalnumDbeads=totalnumDbeads+numDbeads;
                end
        %         hist(distances,1:0.1:50)
            end
        %     CoronadenA(t,:)=totalnumAbeads/aggregatelist(t).numaggs;
        %     CoronadenB(t,:)=totalnumBbeads/aggregatelist(t).numaggs;
        %     CoronadenC(t,:)=totalnumCbeads/aggregatelist(t).numaggs;
        %     CoronadenD(t,:)=totalnumDbeads/aggregatelist(t).numaggs;
            CoronadenA(t,:)=totalnumAbeads/totalaggs;
            CoronadenB(t,:)=totalnumBbeads/totalaggs;
            CoronadenC(t,:)=totalnumCbeads/totalaggs;
            CoronadenD(t,:)=totalnumDbeads/totalaggs;
        end
        CoronadenA=sum(CoronadenA,1)/length(equilstep:length(extract_steps));
        CoronadenB=sum(CoronadenB,1)/length(equilstep:length(extract_steps));
        CoronadenC=sum(CoronadenC,1)/length(equilstep:length(extract_steps));
        CoronadenD=sum(CoronadenD,1)/length(equilstep:length(extract_steps));
        radialshell=0:0.1:100;
        radialvolume=(4/3)*pi*((radialshell.^3)-((radialshell-0.1).^3));
        CoronadenA=CoronadenA./radialvolume;
        CoronadenB=CoronadenB./radialvolume;
        CoronadenC=CoronadenC./radialvolume;
        CoronadenD=CoronadenD./radialvolume;

        figure
        plot(radialshell,CoronadenA,'r-',radialshell,CoronadenB,'b-',radialshell,CoronadenC,'b:',radialshell,CoronadenD,'b--')

        figure
        plot(radialshell,CoronadenA/sum(CoronadenA),'r-',radialshell,CoronadenB/sum(CoronadenB),'b-',radialshell,CoronadenC/sum(CoronadenC),'b:',radialshell,CoronadenD/sum(CoronadenD),'b--')


    end
else
    dencalc=0;
end

if exist('savename')
    savenamebig=[savename '_Big.mat'];
    savename=[savename '.mat'];
    if dencalc==1
        save(savename,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac','radialshell','CoronadenA','CoronadenB','CoronadenC','CoronadenD')
        save(savenamebig,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac','radialshell','CoronadenA','CoronadenB','CoronadenC','CoronadenD','-v7.3')
    else
        save(savename,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac')
        save(savenamebig,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac','-v7.3')
    end
            
            %    save(savenamebig,'aggregatelist','system','system2','systemB','chainstats','chainstats2'','chainstatsB','-v7.3')
end
end

function [distances]=calcdistancesfindrate(coms,pos,dim)
    distances=coms-pos;
    for i=1:3
       distances(distances(:,i)>dim(i)/2,i)=distances(distances(:,i)>dim(i)/2,i)-dim(i);
       distances(distances(:,i)<-dim(i)/2,i)=distances(distances(:,i)<-dim(i)/2,i)+dim(i);
    end
    distances=sort(sum(distances.^2,2)).^0.5;
end
