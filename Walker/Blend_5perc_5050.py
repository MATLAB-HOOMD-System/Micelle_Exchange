#python
import hoomd
import hoomd.md
from hoomd import *
from hoomd import deprecated
hoomd.context.initialize()
polymer1=dict(bond_len=1, type=['A']*2 +['B']*3, bond="linear", count=405)
polymer2=dict(bond_len=1, type=['C']*4 +['D']*3, bond="linear", count=289)
polymer3=dict(bond_len=1, type=['E']*1, bond="linear", count=76952)
hoomd.deprecated.init.create_random_polymers(box=data.boxdim(Lx=30,Ly=30,Lz=30), polymers=[polymer1, polymer2, polymer3], separation=dict(A=0.035, B=0.035, C=0.035, D=0.035, E=0.035), seed=100)
nl = hoomd.md.nlist.cell();
dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=1, seed=100);
dpd.pair_coeff.set('A','A', A=25.0, gamma=3.0)
dpd.pair_coeff.set('A','B', A=50.0, gamma=3.0)

dpd.pair_coeff.set('C','C', A=25.0, gamma=3.0)
dpd.pair_coeff.set('C','D', A=50.0, gamma=3.0)
dpd.pair_coeff.set('D','D', A=25.0, gamma=3.0)

dpd.pair_coeff.set('E','A', A=50.0, gamma=3.0)
dpd.pair_coeff.set('E','B', A=25.0, gamma=3.0)
dpd.pair_coeff.set('E','C', A=50.0, gamma=3.0)
dpd.pair_coeff.set('E','D', A=25.0, gamma=3.0)
dpd.pair_coeff.set('E','E', A=25.0, gamma=3.0)

dpd.pair_coeff.set(['B','D','E'],['B','D','E'], A=25.0, gamma=3.0)
dpd.pair_coeff.set(['B','C','E'],['B','C','E'], A=25.0, gamma=3.0)
dpd.pair_coeff.set(['B','A','E'],['B','A','E'], A=25.0, gamma=3.0)
dpd.pair_coeff.set(['A','C','E'],['A','C','E'], A=25.0, gamma=3.0)
dpd.pair_coeff.set(['A','D','E'],['A','D','E'], A=25.0, gamma=3.0)

nl.reset_exclusions(exclusions = [])
harmonic = hoomd.md.bond.harmonic();
harmonic.bond_coeff.set('polymer', k=100.0, r0=1)
hoomd.md.integrate.mode_standard(dt=.04)
all = hoomd.group.all();
integrator = hoomd.md.integrate.nve(group=all);
integrator.randomize_velocities(kT=1, seed=100)
hoomd.analyze.log(filename="log-output.log", quantities=['potential_energy', 'temperature'], period=500, overwrite=True)

groupA=group.type(name='groupA', type='A')
groupB=group.type(name='groupB', type='B')
group1=group.union(name="1", a=groupA, b=groupB)

groupC=group.type(name='groupC', type='C')
groupD=group.type(name='groupD', type='D')
group2=group.union(name="2", a=groupC, b=groupD)

group3=group.union(name="3", a=group1, b=group2)

hoomd.deprecated.dump.xml(group=group.all(), filename="Blend1.xml", vis=True)
hoomd.dump.dcd("Blend1.dcd", period=1000, group=group3, overwrite=True)
hoomd.run(8e6)
