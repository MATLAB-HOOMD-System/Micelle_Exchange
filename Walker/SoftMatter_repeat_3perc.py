#python
import hoomd
import hoomd.md
from hoomd import *
from hoomd import deprecated
hoomd.context.initialize()
polymer1=dict(bond_len=1, type=['A']*2 +['B']*3, bond="linear", count=486)
polymer2=dict(bond_len=1, type=['C']*1, bond="linear", count=78570)
hoomd.deprecated.init.create_random_polymers(box=data.boxdim(Lx=30,Ly=30,Lz=30), polymers=[polymer1, polymer2], separation=dict(A=0.035, B=0.035, C=0.035), seed=100)
nl = hoomd.md.nlist.cell();
dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=1, seed=100);
dpd.pair_coeff.set('A','A', A=25.0, gamma=3.0)
dpd.pair_coeff.set('A','B', A=50.0, gamma=3.0)
dpd.pair_coeff.set('B','B', A=25.0, gamma=3.0)
dpd.pair_coeff.set('C','A', A=50.0, gamma=3.0)
dpd.pair_coeff.set('C','B', A=25.0, gamma=3.0)
dpd.pair_coeff.set('C','C', A=25.0, gamma=3.0)
nl.reset_exclusions(exclusions = [])
harmonic = hoomd.md.bond.harmonic();
harmonic.bond_coeff.set('polymer', k=100.0, r0=1)
hoomd.md.integrate.mode_standard(dt=.04)
all = hoomd.group.all();
integrator = hoomd.md.integrate.nve(group=all);
integrator.randomize_velocities(kT=1, seed=100)
hoomd.analyze.log(filename="log-output.log", quantities=['potential_energy', 'temperature'], period=500, overwrite=True)
groupA=group.type(name='groupA', type='A')
groupB=group.type(name='groupB', type='B')
groupAB=group.union(name="ab", a=groupA, b=groupB)
#hoomd.deprecated.dump.xml(group=group.all(), filename="SM003.xml", vis=True)
hoomd.dump.dcd("SM003.dcd", period=1000, group=groupAB, overwrite=True)
hoomd.run(8e6)
