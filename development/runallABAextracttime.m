addpath('~/MATLAB/matdcd-1.0')
path='/media/andrew/External_Data/Google Drive LATECH/micelle_dynamics/ABA_vary_conc_0.001_0.02_12-18-17'
path=[path '/'];
addpath(path)
files = dir(path);
files=files (3:end);
runfiles={};
counter=1;

for i=1:length(files)
    name=files(i).name;
    if name(end-2:end)=='.py'
        prefix=name(1:end-3);
        runfiles{counter}=prefix;
        counter=counter+1;
    end
end

celldisp(runfiles)

exclude=[8 9 10];
 

for i=setxor(1:length(runfiles),exclude)
    prefix=runfiles{i};
    xmlname=[prefix '_start.xml'];
    dcdname=[prefix '_fasttraj.dcd'];
    step=10;
    deltat=0.04*1000*step;
    savename=prefix;
    numworkers=12;
    saveflag=1;
    maxsteps=readdcd(dcdname,0,1);
    extract_steps=1:step:maxsteps;
    equilstep=1;
    mixedtype=1;
    cutoff=1;%%%%%%%%%%%%%%%%%%%CHANGE
    
    cprintf('_blue',  prefix);
    [aggregatelist,I,R,Mn,time,tau]=FindRate(xmlname,dcdname,deltat,savename,path,numworkers,saveflag,extract_steps,equilstep,mixedtype,cutoff)
end


% for i=setxor(1:length(runfiles),exclude)
%     clearvars -except path files runfiles counter i exclude
%     prefix=runfiles{i}
%     load([prefix '.mat']);
%     equilstep=find(Mn>mean(Mn(end-99:end))*0.95,1);
%     extract_steps=extract_steps(equilstep:end);
%     aggregatelist=aggregatelist(equilstep:end);
%     I=I(equilstep:end);
%     R=R(equilstep:end);
%     Mn=Mn(equilstep:end);
%     time=time(equilstep:end);
%     time=time-time(1);
%     equilstepave=equilstep;
%     equilstep=1;
%     Recalc_I_R_aveI
%     Rave=R;
%     Iave=I;
%     timeave=time;
%     Mnave=mean(Mn);
%     
%     save([prefix '.mat'],'Rave','Iave','timeave','equilstepave','Mnave','-append');
% end
