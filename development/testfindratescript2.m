addpath('~/MATLAB/matdcd-1.0')
% addpath('/media/andrew/External_Data/Google Drive LATECH/micelle dynamics/ABA_vary_conc')
path='/media/andrew/External_Data/Google Drive LATECH/micelle_dynamics/ABA_vary_conc_0.001_0.02_12-18-17';

prefix='ABA_varyconc_12-18_size80_frac0.001';
xmlname=[prefix '_start.xml'];
dcdname=[prefix '_fasttraj.dcd'];
step=100;
deltat=0.04*100*step;
savename='testsave1';
numworkers=4;
saveflag=1;
extract_steps=4000:100:8000
equilstep=1;
mixedtype=1;


% system=Extract_XML_DCD(xmlname,dcdname,0,0);

[aggregatelist,I,R,Mn,time,tau]=FindRate(xmlname,dcdname,deltat,savename,path,numworkers,saveflag,extract_steps,equilstep,mixedtype)