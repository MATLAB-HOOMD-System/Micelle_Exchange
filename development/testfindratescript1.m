
% addpath('/media/andrew/External_Data/Google Drive LATECH/micelle dynamics/ABA_vary_conc')
path='/media/andrew/External_Data/Google Drive LATECH/UMN_Data/Important Simulation Data/BAB_10-20-16/';
prefix='BAB_10-20-16_B0A12B24_a33_1';
xmlname=[prefix '_strippedstart.xml'];
dcdname=[prefix '_fasttraj.dcd'];
deltat=0.04*100;
savename='testsave1';
numworkers=16;
saveflag=1;
extract_steps=1:100:3900
equilstep=1;
mixedtype=1;


[aggregatelist,I,R,Mn,time,tau]=FindRate(xmlname,dcdname,deltat,savename,path,numworkers,saveflag,extract_steps,equilstep,mixedtype)