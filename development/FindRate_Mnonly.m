function [aggregatelist,I,R,Mn,time,tau]=FindRate(xmlname,dcdname,deltat,savename,path,numworkers,saveflag,extract_steps,equilstep,mixedtype)

%nsteps=[start step
% % addpath('~/submissions/A2B3a55_8_12_16')
if exist('path')~=0
    if path ~=0
        addpath(path)
    end
end
addpath('matlabcode')
addpath('matlabcode/matdcd-1.0')
% clear all
% totsteps=nsteps(end);
% deltat=0.04*10000;

% clc
% system=Extract_XML_DCD('A2B3a55_8_12_16_1_start.xml','A2B3a55_8_12_16_1_reallyfasttraj.dcd',0,0);

system=Extract_XML_DCD(xmlname,dcdname,0,extract_steps);
if mixedtype==1
    system=strip_beads(0,system,'A-B')
elseif mixedtype==2
    system.attype(system.attype=='C' | system.attype=='E' )='B';
    system.attype(system.attype=='D')='A';
    system=strip_beads(0,system,'A-B')
elseif mixedtype==3
    system.attype(system.attype=='A' | system.attype=='B' )='Z';
    system.attype(system.attype=='C' | system.attype=='E' )='B';
    system.attype(system.attype=='D')='A';
    system=strip_beads(0,system,'A-B')
elseif mixedtype==3
    system.attype(system.attype=='C')='A';
    system.attype(system.attype=='D')='B';
    system.attype(system.attype=='E')='B';
    system.attype(system.attype=='F')='C';
    system=strip_beads(0,system,'A-B')
end
% system=trimsystem(system,1000)
system2=system;
systemexpand_script;

% pos=packing(pos(:,:,:),bonds,dim,0) ;
system=systemcompress(attype,bonds,dim,mass,pos,angles);
systemB=strip_beads(0,system,'B');  %'B'
system=strip_beads(0,system);
systemexpand_script;

chainstats=Chain_Statistics_single(0,system);
chainstats2=Chain_Statistics_single(0,system2);
% chainstats2all=Chain_Statistics(0,system2);
chainstatsB=Chain_Statistics_single(0,systemB);
% chainstats=Chain_Statistics(0,system);
systemcompress_script

% delete(gcp('nocreate'))
posall=pos;
[a,b,t]=size(pos);
% if exist('numworkers')
%     if numworkers~=0
%         P=parpool(numworkers)
%     end
% end
% load('test')
% system=systemcompress(attype,bonds,dim,mass,pos,angles);

counter=0;



% needed to make parfor work right
attype=attype;
bonds=bonds;
dim=dim;
mass=mass;
angles=angles;
chainstats=chainstats;

 disp(['starting  ' xmlname])

%  save('FindRateVars')
parfor i=1:length(extract_steps) %%parfor  %%%%aggregate list I think%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp(['start timestep' num2str(i)])
    starttic=tic;
    if mod(i,100)==0
        disp([num2str(i) '  ' xmlname])
    end
    pos=posall(:,:,i);
    system=systemcompress(attype,bonds,dim,mass,pos,angles,chainstats);
    chainstats2=Chain_Statistics_single(0,system);
    aggregatelist(i)=calc_aggregates_beads(0,system);
    aggregatelist(i).chainstats=chainstats2;
    disp(['end timestep' num2str(i)])
    toc(starttic)
%     aggregatelist2=aggregatelist;
%     save('testagglist','aggregatelist2')
end
% disp('pausing now')
% pause(1e6)

% calculate B chainstats
parfor i=1:length(extract_steps)
    systemB2=systemB;
    systemB2.pos=systemB2.pos(:,:,i)
    chainstatsB=Chain_Statistics_single(0,systemB2);
    aggregatelist(i).chainstatsB=chainstatsB;
end


% system=systemcompress(attype,bonds,dim,mass,posall,angles,chainstats);

% aggregatelist(1)
% aggregatelist(400)

for i=1:length(aggregatelist)
    Mn(i)=aggregatelist(i).Mn;
end


%%%calculate distribution of Mn
[num_occurances,Nagg_bin]=hist(([aggregatelist(equilstep:end).agglen]),1:100);
figure
plot(Nagg_bin(1:30),num_occurances(1:30))
xlabel('Nagg')
ylabel('#')
ylim([0 100])

%%%%%%%%calculate unimer fraction
% totalsteps=length(aggregatelist(equilstep:end));
unimerfrac=num_occurances(1)/sum(num_occurances.*Nagg_bin);


if exist('savename')
    savenamebig=[savename '_Big.mat'];
    savename=[savename '.mat'];
    if dencalc==1
        save(savename,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac','radialshell','CoronadenA','CoronadenB','CoronadenC','CoronadenD')
        save(savenamebig,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac','radialshell','CoronadenA','CoronadenB','CoronadenC','CoronadenD','-v7.3')
    else
        save(savename,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac')
        save(savenamebig,'system','system2','systemB','chainstats','chainstats2','chainstatsB','aggregatelist','I','R','Mn','time','tau','deltat','equilstep','extract_steps','Iendaveraged','num_occurances','Nagg_bin','unimerfrac','-v7.3')
    end
            
            %    save(savenamebig,'aggregatelist','system','system2','systemB','chainstats','chainstats2'','chainstatsB','-v7.3')
end
end

function [distances]=calcdistancesfindrate(coms,pos,dim)
    distances=coms-pos;
    for i=1:3
       distances(distances(:,i)>dim(i)/2,i)=distances(distances(:,i)>dim(i)/2,i)-dim(i);
       distances(distances(:,i)<-dim(i)/2,i)=distances(distances(:,i)<-dim(i)/2,i)+dim(i);
    end
    distances=sort(sum(distances.^2,2)).^0.5;
end
