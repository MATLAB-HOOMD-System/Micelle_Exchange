addpath('~/MATLAB/matdcd-1.0')
path='/media/andrew/External_Data/Google Drive LATECH/micelle_dynamics/ABA_vary_conc_0.001_0.02_12-18-17'
path=[path '/'];
addpath(path)
files = dir(path);
files=files (3:end);
runfiles={};
counter=1;
clear listfrac

for i=1:length(files)
    name=files(i).name;
    if name(end-2:end)=='.py'
        prefix=name(1:end-3);
        runfiles{counter}=prefix;
        counter=counter+1;
    end
end

% celldisp(runfiles)

exclude=[8 9 10];

runarray=setxor(1:length(runfiles),exclude);

% counter=1;
for i=runarray
    A=runfiles{i};
    frac=str2num(A(strfind(A,'frac')+4:end));
    listfrac(i)=frac;
end
[Asorted,Aorder]=sort(listfrac);
runfiles=runfiles(Aorder);
runfiles=runfiles(Asorted~=0);

celldisp(runfiles)

for i=1:length(runfiles)
    close all
    prefix=runfiles{i};
    savename=[prefix '.mat'];
    load(savename)
    
    figure('Name','R','Position',[0 2800 350*2 300*2])
    plot(timeave,Rave)
    xlabel('timeave')
    ylabel('Rave')
    title(prefix(strfind(prefix,'frac'):end))
    axis([0 max(timeave) 0.1 1])
    set(gca, 'YScale', 'log')
%     pause(0.00001)

    figure('Name','I','Position',[700 2800 350*2 300*2])
    plot(timeave,Iave)
    xlabel('timeave')
    ylabel('Iave')
    title(prefix(strfind(prefix,'frac'):end))
    axis([0 max(timeave) 0.1 1])
    set(gca, 'YScale', 'log')
%     pause(0.00001)
    
    figure('Name','Mn','Position',[0 500 350*2 300*2])
    plot(time,Mn)
    xlabel('time')
    ylabel('Mn')
    title(prefix(strfind(prefix,'frac'):end))
%     pause(0.00001)
    
    figure('Name','Nbin','Position',[700 500 350*2 300*2])
    plot(Nagg_bin,num_occurances)
    xlabel('N')
    ylabel('#')
    ylim([0 500])
    title(prefix(strfind(prefix,'frac'):end))
%     pause(0.00001)

    Mnave=mean(Mn(400:end))

    if i~=length(runfiles)
        pause
    end
end