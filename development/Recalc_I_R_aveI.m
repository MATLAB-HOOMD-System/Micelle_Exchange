function Write_PCND_XML(File_Name,Output_Name,beadsperchain)

% Writes an XML file from a .mat file, File_Name. It will be saved as
% "Output_Name". Make sure "Output_Name" has a .xml if you want an
% extension.

load(File_Name);
xdim=dim(1);
ydim=dim(2);
zdim=dim(3);

[nat col]=size(pos);
[nangle col]=size(angles);
[nbond col]=size(bonds);


A=attype(:,:)=='A';
x=attype(:,:)=='C';
A=A+x;
B=attype(:,:)=='B';
x=attype(:,:)=='D';
B=B+x;
mass=95.717*A+99.57*B;
diameter=ones(nat,1);
body=-1*ones(nat,1);

fid=fopen(Output_Name,'w','l');

fprintf(fid,'<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid,'<hoomd_xml version="1.4">\n');
fprintf(fid,'<configuration time_step="0" dimensions="3" natoms="%1.0f" >\n',nat);
fprintf(fid,'<box lx="%1.4f" ly="%1.4f" lz="%1.4f"/>\n',[xdim ydim zdim]);

fprintf(fid,'<position num="%1.0f">\n',nat);
fprintf(fid,'%8.4f %8.4f %8.4f\n',pos');
fprintf(fid,'</position>\n');

fprintf(fid,'<mass num="%1.0f">\n',nat);
fprintf(fid,'%6.3f\n',mass);
fprintf(fid,'</mass>\n');

fprintf(fid,'<diameter num="%1.0f">\n',nat);
fprintf(fid,'%3.1f\n',diameter);
fprintf(fid,'</diameter>\n');

fprintf(fid,'<type num="%1.0f">\n',nat);
for i=1:nat
   fprintf(fid,'%1s\n',attype(i,1));
end
fprintf(fid,'</type>\n');

fprintf(fid,'<body num="%1.0f">\n',nat);
fprintf(fid,'%2.0f\n',body);
fprintf(fid,'</body>\n');

fprintf(fid,'<bond num="%1.0f">\n',nbond);
for i=1:nbond
    if attype(bonds(i,1)+1,1)=='A'
        fprintf(fid,'polymer %1.0f %1.0f\n',bonds(i,:));
    elseif attype(bonds(i,1)+1,1)=='B'
        fprintf(fid,'polymer2 %1.0f %1.0f\n',bonds(i,:));
    elseif attype(bonds(i,1)+1,1)=='C'
        fprintf(fid,'polymer3 %1.0f %1.0f\n',bonds(i,:));
    else
        fprintf(fid,'polymer4 %1.0f %1.0f\n',bonds(i,:));
    end
end
fprintf(fid,'</bond>\n');

fprintf(fid,'<angle num="%1.0f">\n',nangle);
for i=1:nangle
    if attype(angles(i,2)+1,1)=='A'
        fprintf(fid,'A-A-A %1.0f %1.0f %1.0f\n',angles(i,:));
    elseif attype(angles(i,2)+1,1)=='B'
        fprintf(fid,'B-B-B %1.0f %1.0f %1.0f\n',angles(i,:));
    elseif attype(angles(i,2)+1,1)=='C'
        fprintf(fid,'C-C-C %1.0f %1.0f %1.0f\n',angles(i,:));
    else
        fprintf(fid,'D-D-D %1.0f %1.0f %1.0f\n',angles(i,:));
    end
end
b=0;
p=1;
for i=1:nangle
    fprintf(fid,'polymer%1.0f %1.0f %1.0f %1.0f\n',[p angles(i,:)]);
    b=b+1;
    if mod(b,beadsperchain-2)==0
        p=p+1;
    end
end

fprintf(fid,'</angle>\n');

fprintf(fid,'</configuration>\n');
fprintf(fid,'</hoomd_xml>\n');
fclose(fid);
% toc
end