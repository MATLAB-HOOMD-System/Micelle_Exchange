function aggregatelist=calc_aggregates_beads(FileName,system,cutoff,AggComFlag)

% if exist('cutoff','var')==0
%     cutoff=1.5;
% end
% Extract_XML(FileName,'agg')
if FileName~=0
    load(FileName)
else
    [attype,bonds,dim,mass,pos,angles, chainstats]=systemexpand(system);
end
system.angles=ones(0,3);
system.bonds=[];
chainstats=Chain_Statistics_single(0,system)
system=systemcompress(attype,bonds,dim,mass,pos,angles);
system.chainstats=chainstats

% pos=packing(pos(:,:,:),bonds,dim,0) ;
% 
% % savename='agg';
% 
% % save_simulation
% 
% system=systemcompress(attype,bonds,dim,mass,pos,angles);
% system=strip_beads(0,system);
% systemexpand_script;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load('stripped')
% system=systemcompress(attype,bonds,dim,mass,pos,angles);
if exist('AggComFlag','var')==0
    AggComFlag=0;
end

[aggregatelist,chainstats]=calc_aggregates(0,system,cutoff,AggComFlag);
% [aggregatelist2,chainstats]=calc_aggregates_backup(0,system);
% aggregatelist
% aggregatelist2
% aggregatelist.Mn/aggregatelist2.Mn
% 
% 1==1


% load('chainstats')

% a=size(aggregatelist.aggregates); %a=largest aggregate, numaggs
% maxagg=a(1);
% s=size(chainstats.chains2);
% for i=1:aggregatelist.numaggscalc_aggregates_beads
%     counter=1;
%     for j=1:maxagg
%     if aggregatelist.aggregates(j,i)~=0
%         chainnum=aggregatelist.aggregates(j,i);
%         for k=1:s(1)
%             if chainstats.chains2(k,chainnum)~=0
%                 aggregatelist.aggbeads(counter,i)=chainstats.chains2(k,chainnum);
%                 counter=counter+1;
%             end
%         end
%         
%     end
%     end
% end
    


% counter=1;
% for i=29:29
%     i
%     aggregatelist=calc_aggregates('stripped',i)
%     numaggs(counter)=aggregatelist.numaggs;
%     Mn(counter)=aggregatelist.Mn;
%     Mw(counter)=aggregaaggregates=unique(aggregates','rows');telist.Mw;
%     counter=counter+1;
% end
% 
% plot(numaggs)
% hold on
% plot(Mn)
% plot(Mw)
% 
% Write_XML('stripped','stripped.xml')
