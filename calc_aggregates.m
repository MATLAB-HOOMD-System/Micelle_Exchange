function [aggregatelist,chainstats]=calc_aggregates(FileName,system,cutoff,AggComFlag)
% disp('startingaggcalc')
% if exist('cutoff','var')==0
%     cutoff=1.5;
% end
tic
if FileName~=0
    load(FileName);
else
    [attype,bonds,dim,mass,pos,angles,chainstats]=systemexpand(system);
end

posall=pos;
s=size(pos);
if exist('angles')==0
    angles=ones(0,3);
end

timesteps=1;
% if length(size(pos))==2
%     timesteps=1;
% end
%
% if exist('timesteps')==0
%     timesteps=1:s(3);
% end
%
% if timesteps==0
%     timesteps=1:s(3);
% end

timestepoffset=1-timesteps(1);
for t=timesteps
    clear agglist agg1 aggregates agglen
    pos=posall(:,:,t);
% chainstats=Chain_Statistics(0,system);
% save('chainstats','chainstats')

for i=1:chainstats.nchains
    chainCOM(i,:)=COM(pos(chainstats.chains2(:,i),:),dim);
end
% disp('after COM before distances')
% showmemorytotal
% toc
% tic;

% squareform(pdist(chainCOM))
% distances=(pdist(chainCOM));
%%%%%%%%%%%%%%%%%%%%distance calc

%xdistances
distances=(pdist(pos(:,1)));
distances(distances>dim(1)/2)=dim(1)-distances(distances>dim(1)/2);
distances=distances.^2;

% disp('after x distances')
% showmemorytotal

%ydistances
tempdistances=(pdist(pos(:,2)));
tempdistances(tempdistances>dim(2)/2)=dim(2)-tempdistances(tempdistances>dim(2)/2);
distances=distances+tempdistances.^2;
clear tempdistances

% disp('after y distances')
% showmemorytotal

%zdistances
tempdistances=(pdist(pos(:,3)));
tempdistances(tempdistances>dim(3)/2)=dim(3)-tempdistances(tempdistances>dim(3)/2);
distances=distances+tempdistances.^2;
clear tempdistances

% disp('after z distances')
% showmemorytotal

% distances=(xdistances.^2+ydistances.^2+zdistances.^2).^(1/2);

% disp('after all distances')
% showmemorytotal
% save('memoryusagedistances')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%cutoff=1;
% distancescutoff=distances<cutoff;
% sqaredistancescutoff=squareform(distancescutoff);
distances=distances.^(1/2);
distances=distances<cutoff;
distances=squareform(distances);

% disp('after distance before agglist')
% showmemorytotal
% toc
% tic;
% disp('donedistances')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%find aggregates
% firstagg=tic;
totaltest=1;
totalcounter=0;
j=1;
while totaltest==1
    
% for j=1:size(pos,1)
% j
totalcounter=totalcounter+1;
    if j==1 
        runflag=1;
    elseif sum(sum(sum(aggregates==j)))==0
        runflag=1;
    else
        runflag=0;
    end
%     runflag=1;
    if runflag==1
%         if sum(sum(sum(aggreggates)))==0
            agg1=find(distances(j,:),1e20);
            agglist=[j agg1];
            aggtest=1;
            oldagglist=agglist;
            oldagglist2=1;
            while aggtest==1
                if size(pos,1)~=size(agglist,1)
                    for i=agglist(~ismember(agglist,oldagglist2))
                        tempfindlist=find(distances(i,:),1e20);
                        tempfindlist2=chainstats.chains2(:,chainstats.chain(i))';
                        agglist=[agglist tempfindlist(~ismember(tempfindlist,agglist))];
                        agglist=[agglist tempfindlist2(~ismember(tempfindlist2,agglist))];
    %                     agglist=[agglist chainstats.chains2(:,chainstats.chain(i))'];
    %                     agglist=unique(agglist);
                    end
                
%                 agglist=unique(agglist);
                if length(agglist)==length(oldagglist)
                    if agglist==oldagglist
                        aggtest=0;
                    end
                end
                oldagglist2=oldagglist;
                oldagglist=agglist;
%                 system.pos(agglist,:)
                else
                    aggtest=0;
                end
            end
            if j==1
                aggregates(:,1)=agglist;
            else
                [largestagg,numaggs]=size(aggregates);
                if largestagg<length(agglist)
                    aggregates(largestagg+1:length(agglist),:)=0;
                    aggregates(:,totalcounter)=agglist';
                elseif largestagg==length(agglist)
                    aggregates(:,totalcounter)=agglist';
                else
                    aggregates(:,totalcounter)=[agglist'; zeros(largestagg-length(agglist),1)];
                end
            end
        end
        
    j=find(~ismember(j:size(pos,1),aggregates),1)+j-1;
    if isempty(j)
        totaltest=0;
    end
end
% disp('afteraggfind')
% showmemorytotal
% toc(firstagg)
% tic;
% disp('pausing here')
% pause(1e6)

% aggregates=sort(unique(aggregates','rows')');
aggregates=sort(aggregates);
% if aggregates(1,1)==0
%     aggregates=aggregates(:,2:end);
% end
chains=0*aggregates;
agginfo=size(aggregates);
for j=1:agginfo(2)
    filter=aggregates(aggregates(:,j)~=0,j);
    chains1=unique(chainstats.chain(filter));
    chains(1:length(chains1),j)=chains1;
end

chains=chains(1:max(sum(chains~=0)),:);




%     size(aggregates)
    aggbeads=aggregates;
    aggregates=chains;

%     aggregates=aggregates';
    [largestagg,numaggs]=size(aggregates);

    for i=1:numaggs
        agglen(i)=length(find(aggregates(:,i),1e20));
    end

%     [oldlargestagg,oldnumagg,oldtimesteps]=aggregatelist.aggregates;
    [largestagg,numagg]=size(aggregates);

    aggregatelist.aggregates(:,:,t+timestepoffset)=aggregates;
    aggregatelist.agglen(:,:,t+timestepoffset)=agglen;
    aggregatelist.numaggs=numagg;
    aggregatelist.Mn=mean(agglen);
    aggregatelist.aggbeads=aggbeads;
    aggregatelist.chainstats=chainstats;
%     aggregatelist.Mw=mean(agglen);
    num=0;
    denom=0;
    for i=1:100
        num=num+i*i*sum(agglen==i);
        denom=denom+i*sum(agglen==i);
    end
    aggregatelist.Mw=num/denom;
    
    if exist('AggComFlag')==1
        if AggComFlag==1
            for i=1:aggregatelist.numaggs
                beadlist=aggregatelist.aggregates(:,i);
                beadlist=beadlist(beadlist~=0);
                aggregatelist.AggCOM(i,:)=COM(pos(beadlist,:),dim);
                if aggregatelist.AggCOM(i,1)<-dim(1)/2
                    aggregatelist.AggCOM(i,1)=aggregatelist.AggCOM(i,1)+dim(1);
                elseif aggregatelist.AggCOM(i,1)>dim(1)/2
                    aggregatelist.AggCOM(i,1)=aggregatelist.AggCOM(i,1)-dim(1);
                end
                if aggregatelist.AggCOM(i,2)<-dim(2)/2
                    aggregatelist.AggCOM(i,2)=aggregatelist.AggCOM(i,2)+dim(2);
                elseif aggregatelist.AggCOM(i,2)>dim(2)/2
                    aggregatelist.AggCOM(i,2)=aggregatelist.AggCOM(i,2)-dim(2);
                end
                if aggregatelist.AggCOM(i,3)<-dim(3)/2
                    aggregatelist.AggCOM(i,3)=aggregatelist.AggCOM(i,3)+dim(3);
                elseif aggregatelist.AggCOM(i,3)>dim(3)/2
                    aggregatelist.AggCOM(i,3)=aggregatelist.AggCOM(i,3)-dim(3);
                end
            end
    end

% disp(['afterall'])
% showmemorytotal
% toc
% pause(1e6)
end
end
